# nuxt-svg-module

> My wonderful Nuxt.js project

## Build Setup

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).

# Install global SVG icon loader

[@nuxtjs/svg](https://github.com/nuxt-community/svg-module)

```bash
yarn add @nuxtjs/svg
```

```js
export default {
    plugins: [
    { src: '@/plugins/vue-icons-loader', mode: 'client' }
  ],
modules: [
    '@nuxtjs/svg'
  ],
}
```

```js
import Vue from 'vue'

const components = require.context('@/assets/icons/.?inline', false, /\.svg$/)

components.keys().forEach((fileName) => {
  const componentConfig = components(fileName)
  const componentName = fileName
    .split('/')
    .pop()
    .split('.')[0]

  Vue.component(
    `icon-${componentName}`,
    componentConfig.default || componentConfig
  )
})
```

```bash
📦 repository
 ┣ 📂 assets
 ┃ ┣ 📂icons
 ┃ ┣ ┗📜icon.svg
 ┣ 📂 components
```

```bash
 📦 repository
 ┣ 📂 assets
 ┃ ┣ 📂icons
 ┃ ┣ ┗📜icon.svg
 ┣ 📂 components
 ┃ ┣ 📂base
 ┃   ┗ 📜icon-base.vue
```

```js
export default {
    plugins: [
    { src: '@/plugins/global-components-loader' },
  ],
}
```

```js
import Vue from 'vue'

const components = require.context(
  '@/components/base',
  true,
  /[a-z]\w+\.(vue)$/
)

components.keys().forEach((fileName) => {
  const componentConfig = components(fileName)
  const componentName = fileName
    .split('/')
    .pop()
    .split('.')[0]

  Vue.component(componentName, componentConfig.default || componentConfig)
})
```

```vue
<template>
  <span
    :class="[{ 'svg-baseline': baseline }, `icon-${name}`]"
    class="svg-icon"
  >
    <client-only>
      <!-- eslint-disable vue/require-component-is -->
      <component
        :is="`icon-${name}`"
        :width="width"
        :height="height"
        :class="variant"
      >
      </component>
      <div v-if="text" class="position-relative">
        <div class="text">
          {{ text }}
        </div>
      </div>

      <span v-if="badge" class="badge" v-text="badge"></span>
    </client-only>
  </span>
</template>

<script>
export default {
  name: 'IconBase',
  props: {
    name: {
      type: String,
      required: true,
      default: 'secob'
    },
    baseline: {
      type: Boolean,
      default: false
    },
    width: {
      type: [Number, String],
      default: 18
    },
    height: {
      type: [Number, String],
      default: 18
    },
    variant: {
      type: String,
      default: 'default'
    },
    badge: {
      type: [String, Number],
      default: ''
    },
    text: {
      type: String,
      default: ''
    }
  }
}
</script>

<style lang="scss" scoped>
.svg-icon {
  position: relative;
  display: inline-flex;
  align-self: center;

  &.svg-baseline svg {
    top: 0.125em;
    position: relative;
  }
}

.text {
  position: absolute;
  z-index: 1;
  top: 50%;
  left: -3rem;
  transform: translate(-50%, -50%);
}

.badge {
  font-size: 0.7rem;
  position: absolute;
  top: -0.5rem;
  right: -0.5rem;
}

svg {
  &.default {
    fill: black;
  }
  &.white {
    fill: white;
  }
  &.success {
    fill: #6ae277;
  }
}
</style>
```
